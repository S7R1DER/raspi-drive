const express = require('express'),
  morgan = require('morgan'),
  helmet = require('helmet'),
  cors = require('cors'),
  fileUpload = require('express-fileupload'),
  zip = require('express-easy-zip'),
  middlewares = require('./middlewares'),
  apiv1 = require('./api/v1');

require('dotenv').config();

const app = express();

app.use(fileUpload());
app.use(helmet({
  referrerPolicy: { policy: 'no-referrer' },
}));
app.use(cors());
app.use(zip());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan('dev'));

app.use('/api/v1', apiv1);

app.use(middlewares.notFound);
app.use(middlewares.errorHandler);

module.exports = app;

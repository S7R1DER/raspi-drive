const express = require('express'),
  handlers = require('../controller/fileHandlers');

const router = express.Router();

// uploads
router.post('/:path*', (req, res) => {
  console.log(req.files);
  const { path } = req.params,
    { filesToUpload } = req.files;

  handlers.uploadFiles(filesToUpload, path + req.params[0])
    .then((data) => res.status(201).json(data))
    .catch((err) => res.status(400).json(err));
});

// creating directories
router.put('/:path*', (req, res) => {
  const { path } = req.params;
  handlers.createDirectory(path + req.params[0])
    .then((directory) => res.status(201).json({ path: directory }))
    .catch((err) => res.status(409).json(err));
});

// listing and downloading
router.get('/download', (req, res) => {
  const { mode, path } = req.query;

  if (mode === 'single') {
    handlers.downloadFile(path)
      .then((file) => res.download(file))
      .catch((err) => res.status(404).json(err));
  } else if (mode === 'multiple') {
    handlers.downloadFiles(path)
      .then((data) => res.zip(data))
      .catch((err) => res.status(404).json(err));
  } else {
    res.sendStatus(400);
  }
});

router.get('/', (_req, res) => {
  handlers.getFiles('')
    .then((files) => res.json(files))
    .catch((err) => res.json(err));
});

router.get('/:path*', (req, res) => {
  const { path } = req.params;

  handlers.getFiles(path + req.params[0])
    .then((data) => res.json(data))
    .catch((err) => res.status(404).json(err));
});

// renaming and moving
router.patch('/:path*', (req, res) => {
  const { path } = req.params,
    { to, action } = req.query;

  handlers.renameOrMove(path + req.params[0], to, action)
    .then((file) => res.status(201).json({ path: file }))
    .catch((err) => res.status(400).json(err));
});

// deletions
router.delete('/:path*', (req, res) => {
  const { path } = req.params;

  handlers.remove(path + req.params[0])
    .then(() => res.sendStatus(204))
    .catch((err) => res.status(400).json(err));
});

module.exports = router;

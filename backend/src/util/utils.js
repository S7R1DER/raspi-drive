exports.normalizeSize = (sizeInBytes) => {
  if (sizeInBytes < 1024 ** 2) return `${(sizeInBytes / 1024).toFixed(2)} KB`;
  if (sizeInBytes < 1024 ** 3) return `${(sizeInBytes / 1024 ** 2).toFixed(2)} MB`;
  if (sizeInBytes < 1024 ** 4) return `${(sizeInBytes / 1024 ** 3).toFixed(2)} GB`;
  return `${(sizeInBytes / 1024 ** 4).toFixed(2)} TB`;
};

exports.formatDate = (aDate) => new Date(aDate).toISOString().slice(0, -5).replace(/\D/g, '-');

const fs = require('fs'),
  path = require('path'),
  { normalizeSize, formatDate } = require('../util/utils');

exports.uploadFiles = (files, targetPath) => new Promise((resolve, reject) => {
  try {
    if (!files || Object.keys(files).length === 0) throw new Error('No files submitted');
    if (!targetPath || targetPath === '') throw new Error('Target path not specified');

    const fullPath = path.normalize(path.join(process.env.MOUNT_POINT, targetPath));
    const filesInfo = [];

    const fileArray = files instanceof Array ? files : [files];
    Object.values(fileArray).forEach((file) => {
      const filePath = path.join(fullPath, file.name);
      file.mv(filePath, (err) => {
        if (err) throw err;
      });

      filesInfo.push({
        name: file.name,
        mimetype: file.mimetype,
        path: filePath,
        size: normalizeSize(file.size),
      });
    });

    resolve(filesInfo);
  } catch (err) {
    reject(err);
  }
});

exports.createDirectory = (fromPath) => new Promise((resolve, reject) => {
  try {
    const fullPath = path.normalize(path.join(process.env.MOUNT_POINT, fromPath));

    if (!fs.existsSync(fullPath)) {
      fs.mkdirSync(fullPath);
      resolve(fullPath);
    } else {
      throw new Error('Directory already exists!');
    }
  } catch (err) {
    reject(err);
  }
});

exports.getFiles = (fromPath) => new Promise((resolve, reject) => {
  try {
    const fullPath = path.normalize(path.join(process.env.MOUNT_POINT, fromPath));

    fs.readdir(fullPath, (err, files) => {
      if (err) throw err;

      let fileList = [];
      files.forEach((elem) => {
        const fileInfo = fs.statSync(path.join(fullPath, elem));
        const file = {
          name: elem,
          isDirectory: fileInfo.isDirectory(),
          size: normalizeSize(fileInfo.size),
          extension: path.extname(elem),
          created: fileInfo.birthtime,
          lastModified: fileInfo.mtime,
        };

        fileList = [...fileList, file];
      });
      resolve(fileList);
    });
  } catch (err) {
    reject(err);
  }
});

exports.downloadFile = (fromPath) => new Promise((resolve, reject) => {
  try {
    const fullPath = path.normalize(path.join(process.env.MOUNT_POINT,
      fromPath instanceof Array ? fromPath[0] : fromPath));

    fs.stat(fullPath, (err, stat) => {
      if (err) throw err;
      if (stat.isDirectory()) throw new Error('Functionality NYI for directories');
      resolve(fullPath);
    });
  } catch (err) {
    reject(err);
  }
});

exports.downloadFiles = (fromPaths) => new Promise((resolve, reject) => {
  try {
    const date = new Date();
    const data = {
      files: [
        {
          name: 'RaspiDrive-download-zip',
          mode: 755,
          date,
        },
      ],
      filename: `${formatDate(date)}-raspi-drive.zip`,
    };

    const paths = fromPaths instanceof Array ? fromPaths : [fromPaths];

    paths.forEach((subPath) => {
      const filePath = path.normalize(path.join(process.env.MOUNT_POINT, subPath));
      if (fs.existsSync(filePath)) {
        data.files = [...data.files, {
          path: filePath,
          name: path.basename(filePath),
        }];
      }
    });

    resolve(data);
  } catch (err) {
    reject(err);
  }
});

exports.renameOrMove = (fromPath, targetPath, action = 'rename') => new Promise((resolve, reject) => {
  try {
    if (!targetPath || targetPath === '') throw new Error('New name not specified!');

    const fullPath = path.normalize(path.join(process.env.MOUNT_POINT, fromPath));
    let newPath;
    switch (action) {
      case 'rename':
        newPath = path.normalize(path.join(path.parse(fullPath).dir, targetPath));
        break;
      case 'move':
        newPath = path.normalize(path.join(process.env.MOUNT_POINT, targetPath));
        break;
      default:
        throw new Error('Action not recognised.');
    }

    if (!fs.existsSync(fullPath)) throw new Error('The file to be renamed does not exist!');
    if (fs.existsSync(newPath)) throw new Error('A file already exists with that name!');

    // TODO: fix not renaming certain directories
    fs.renameSync(fullPath, newPath);
    resolve(newPath);
  } catch (err) {
    reject(err);
  }
});

exports.remove = (fromPath) => new Promise((resolve, reject) => {
  try {
    const fullPath = path.normalize(path.join(process.env.MOUNT_POINT, fromPath));

    fs.stat(fullPath, (err, stat) => {
      if (err) throw err;

      if (!stat.isDirectory()) {
        fs.unlinkSync(fullPath);
      } else {
        fs.rmdirSync(fullPath, { recursive: true });
      }

      resolve();
    });
  } catch (err) {
    reject(err);
  }
});

import { fileTypes, icons } from './constants';

export const getFileType = (isDirectory, extension) => {
  if (isDirectory) {
    if (extension === 'back') {
      return fileTypes.BACK;
    }
    return fileTypes.FOLDER;
  }
  return fileTypes[Object.keys(fileTypes)
    .find((key) => fileTypes[key] === extension.replace('.', ''))];
};

export const getIcon = (fileType) => icons[Object.keys(fileTypes)
  .find((key) => fileTypes[key] === fileType)];

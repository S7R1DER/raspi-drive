// eslint-disable-next-line import/prefer-default-export
export const simplifyPath = (path) => path.replace(/\w+\.\.[\\/]{0,2}/, '');
